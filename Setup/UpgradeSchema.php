<?php

namespace Swissclinic\CustomFields\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * UpgradeSchema constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }
    protected function _addAttributes($_attributeCodes, $strEntityType){
        $customerSetup =  $this->customerSetupFactory->create();

        foreach ($_attributeCodes as $attribCode => $params){
            $customerSetup->addAttribute($strEntityType, $attribCode, $params);
        }

    }
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if(version_compare($context->getVersion(),'0.0.3','<=')){

            $_customerFields = $this->_getCustomerAttributes();
            $this->_addAttributes($_customerFields,\Magento\Customer\Model\Customer::ENTITY);
            // Assign customer attributes to group set and default attribute group.
            $this->_addAttributeGroupAndSet($_customerFields,\Magento\Customer\Model\Customer::ENTITY);
        }

        $setup->endSetup();

    }
    /**
     * @param $_attributeCodes
     * @param $strEntityType
     */
    protected function _addAttributeGroupAndSet($_attributeCodes, $strEntityType){

        $customerSetup =  $this->customerSetupFactory->create();
        $customerEntity = $customerSetup->getEavConfig()->getEntityType($strEntityType);

        // get the set id
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        // get the group id
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        foreach ($_attributeCodes as $_attributeCode => $_params){
            $attribute = $customerSetup->getEavConfig()
                ->getAttribute($strEntityType, $_attributeCode)
                ->addData(
                    [
                        'attribute_set_id' => $attributeSetId,
                        'attribute_group_id' => $attributeGroupId,
                        'used_in_forms'=> ['customer_account_create', 'customer_account_edit', 'checkout_register','adminhtml_customer']
                    ]
                );
            $attribute->save();
        }
    }

    public function _getCustomerAttributes(){

        $customerAttributes= array(
            'rg_number'=>[
                'type' => 'varchar',
                'label' => 'RG',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 101,
                'position' => 101,
                'system' => 0,
            ]
        );

        return $customerAttributes;
    }


    }